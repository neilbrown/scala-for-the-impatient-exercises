/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 3. Working with Arrays, Exercise 4.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Given an array of integers, produce a new array that contains all positive integers of the original array, in 
// their original order, followed by all the values that are zero or negative in their original order
package com.neiljbrown.scalaforimpatient.chapter3

object Exercise4 extends App {
  val a = Array(-1, 1, 0, -2, -1, 2, 3, 0, 4, 5, -3)
  val positiveElements = for (elem <- a if elem > 0) yield elem
  val nonPositiveElements = for (elem <- a if elem <= 0) yield elem
  val result = positiveElements ++ nonPositiveElements
  assert("1,2,3,4,5,-1,0,-2,-1,0,-3" == result.mkString(","))
}