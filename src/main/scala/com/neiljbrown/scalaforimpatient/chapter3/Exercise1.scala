/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown 
 * Chapter 3. Working with Arrays, Exercise 1.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write a code snippet that sets 'a' to an array of 'n' random integers between 0 (inclusive) and n (exclusive).
package com.neiljbrown.scalaforimpatient.chapter3

// Random companion (singleton) object requires import of util package
import util._

object Exercise1 extends App {
  val n = 10
  val a = new Array[Int](n)
  for(i <- 0 until n)
    a(i) = Random.nextInt(n)
  println("Generated random array of integers of size [" + a.size + "], values [" +  a.mkString(",") + "]")
}