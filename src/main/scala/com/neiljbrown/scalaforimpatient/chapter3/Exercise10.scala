/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 3. Working with Arrays, Exercise 10.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Import java.awt.datatransfer._ and make an object of type SystemFlavorMap with the call - 
//    val flavors = SystemFlavorMap.getDefaultFlavorMap().asIstanceOf[SystemFlavorMap]
// Then call the getNativesForFlavor method with parameter DataFlavor.imageFlavor and get the return value as a Scala 
// buffer. (Why this obscure class? It's hard to find uses of java.util.List in the standard Java library)
package com.neiljbrown.scalaforimpatient.chapter3

// Import object JavaConversions to support implicit conversion of java.util.List to/from Scala Buffer (ArrayBuffer 
// supertype). Avoids the need to declare java.util.List in your Scala code, allowing you to use Scala Buffers instead
import scala.collection.JavaConversions.asScalaBuffer
import scala.collection.mutable.Buffer
import java.awt.datatransfer._

object Exercise10 extends App {
  // Just call some arbitrary JSE method which returns an object that can subsequently be used to return a Java List
  val flavors = SystemFlavorMap.getDefaultFlavorMap().asInstanceOf[SystemFlavorMap]

  // Call a Java method which returns a List<String> and have Scala automatically convert it to a Buffer
  val natives: Buffer[String] = flavors.getNativesForFlavor(DataFlavor.imageFlavor)
  println("List of AWT natives to which DataFlavor.imageFlavor can be translated [" + natives.mkString(",") + "]")
}