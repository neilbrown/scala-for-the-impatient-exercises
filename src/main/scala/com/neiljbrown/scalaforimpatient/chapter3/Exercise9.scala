/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 3. Working with Arrays, Exercise 9.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Make a collection of all time zones returned by java.util.TimeZone.getAvailableIDs that are in America. Stip off
// the "America/" prefix and sort the result
package com.neiljbrown.scalaforimpatient.chapter3

object Exercise9 extends App {
  // Java arrays, such as String[] as returned by java.util.TimeZone.getAvailableIDs, are implicitly converted to Scala 
  // Array of corresponding type
  val americaZoneSuffix =
    for (tzId <- java.util.TimeZone.getAvailableIDs if tzId.startsWith("America")) yield tzId.stripPrefix("America/")
  val sortedAmericaZoneSuffix = americaZoneSuffix.sortWith(_ < _)

  // This problem can also be solved in a terser manner using Array's filter() and map() methods -
  val sortedAmericzZoneSuffix2 =
    java.util.TimeZone.getAvailableIDs.filter(_.startsWith("America")).map(_.stripPrefix("America/")).sortWith(_ < _)
  // Note, equality does not work for arrays
  assert(sortedAmericaZoneSuffix.mkString(",") == sortedAmericzZoneSuffix2.mkString(","))

  println("America timezone suffixes, sorted [" + sortedAmericaZoneSuffix.mkString(",") + "]")
}