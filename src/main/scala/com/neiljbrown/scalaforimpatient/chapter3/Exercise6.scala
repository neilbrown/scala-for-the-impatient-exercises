/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown 
 * Chapter 3. Working with Arrays, Exercise 6.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) How do you rearrange the elements of an Array[Int] so that they appear in reverse sorted order? How do you do the 
// same with an ArrayBuffer[Int]?
package com.neiljbrown.scalaforimpatient.chapter3

object Exercise6 extends App {
  val a = Array[Int](3, 2, 4, 1, 5)
  // This solution uses ArrayOps.sortWith, passing a comparison function as a parameter, which returns another Array
  // Arrays can also be sorted in place (not requiring creation of another Array) using scala.util.Sorting.quickSort, 
  // however this does not appear to support a comparison function, requiring an implementation of scala.math.Ordering
  val aSorted = a.sortWith(_ > _)
  assert("5,4,3,2,1" == aSorted.mkString(","))

  // ArrayBuffer can be sorted using the same functions as Array, although do not support sorting in place
  val ab = a.toBuffer
  val abSorted = ab.sortWith(_ > _)
  assert("5,4,3,2,1" == abSorted.mkString(","))
}