/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown 
 * Chapter 3. Working with Arrays, Exercise 2.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write a loop that swaps adjacent elements of an array of integers. For example, Array(1, 2, 3, 4, 5) becomes 
// Array(2, 1, 4, 3, 5)
package com.neiljbrown.scalaforimpatient.chapter3

object Exercise2 extends App {
  // The long-winded way
  val a = Array(1, 2, 3, 4, 5)
  println("Given array [" + a.mkString(",") + "]...")
  for (i <- 0 until a.length if i % 2 == 1) {
    val x = a(i - 1)
    a(i - 1) = a(i)
    a(i) = x
  }

  println("...swapped adjacent elements to create array [" + a.mkString(",") + "].")
  assert("2,1,4,3,5" == a.mkString(","))
}