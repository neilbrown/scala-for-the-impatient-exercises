/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown 
 * Chapter 3. Working with Arrays, Exercise 3.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Repeat the preceding assignment, but produce a new array with the swapped values. Use for / yield.
package com.neiljbrown.scalaforimpatient.chapter3

object Exercise3 extends App {
  // The following solution seems a bit verbose. Is there a terser, functional way to do this?
  val a = Array(1, 2, 3, 4, 5)
  println("Given array [" + a.mkString(",") + "]...")
  val result = for (i <- 0 until a.length) yield (
    if (i % 2 == 0 && (i + 1 < a.length)) {
      a(i + 1)
    } else if (i % 2 == 0 && (i + 1 == a.length)) {
      a(i)
    } else {
      a(i - 1)
    })

  println("...swapped adjacent elements to create _new_ array [" + result.mkString(",") + "].")
  assert("2,1,4,3,5" == result.mkString(","))

  // The logic, is actually simpler if the resulting array is created in advance, rather than using yield -
  val b = new Array[Int](a.length)
  for (i <- 0 until a.length) {
    if (i % 2 == 1) {
      b(i) = a(i - 1)
      b(i - 1) = a(i)
    } else if (i == a.length - 1) {
      b(i) = a(i)
    }
  }
  assert("2,1,4,3,5" == b.mkString(","))
}