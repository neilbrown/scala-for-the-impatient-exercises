/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 3. Working with Arrays, Exercise 8.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Rewrite the example at the end of Section 3.4, "Transforming Arrays", on page 34 using the drop method for 
// dropping the index of the first match. Look the method up in Scaladoc
package com.neiljbrown.scalaforimpatient.chapter3

object Exercise8 extends App {
  // Convert array to ArrayBuffer, as Array does not support remove()
  val a = Array(1, -1, 0, -2, -1, 2, 3, 0, 4, 5, -3).toBuffer
  // Calculate indexes of all negative no.s as per the example, but additionally drop the first index using drop(1)
  val indexesToNegatives = (for (i <- 0 until a.length if a(i) < 0) yield i).drop(1)

  // Now, modify the example to process all remaining indexes rather than skipping the first one
  //for (j <- (1 until indexesToNegatives.length).reverse) a.remove(indexesToNegatives(j))
  for (j <- (0 until indexesToNegatives.length).reverse) a.remove(indexesToNegatives(j))
  assert(a.mkString(",") == "1,-1,0,2,3,0,4,5")
}