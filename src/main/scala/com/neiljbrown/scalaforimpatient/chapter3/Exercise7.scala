/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 3. Working with Arrays, Exercise 7.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write a code snippet that produces all values from an array with duplicates removed. (Hint: Look at Scaladoc).
package com.neiljbrown.scalaforimpatient.chapter3

object Exercise7 extends App {
  val a = Array[Int](3, 1, 2, 5, 4, 1, 2, 5, 6, 1)
  val result = a.distinct
  assert("3,1,2,5,4,6" == result.mkString(","))
}