/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 3. Working with Arrays, Exercise 5.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) How do you compute the average of an Array[Double]?
package com.neiljbrown.scalaforimpatient.chapter3

object Exercise5 extends App {
  val a = Array[Double](1.5331, 3.14, 9.20320, 0.0, 2389.484849)
  val ave = a.sum / a.length
  println(ave)
  assert(480.67222979999997 == ave)
}