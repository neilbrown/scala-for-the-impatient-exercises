/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 2. Control Structures and Functions, Exercise 2.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) What is the value of an empty block expression {}? What is its type?
// A) The type of the expression is 'Unit'. Its value is 'no value'.
package com.neiljbrown.scalaforimpatient.chapter2

object Exercise2 extends App {
  val x = {}
  assert(x.isInstanceOf[Unit])
}
