/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 2. Control Structures and Functions, Exercise 6.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write a for loop for computing the product of the Unicode codes of all letters in a string. For example, the 
// product of the characters in "Hello" is 825152896.
package com.neiljbrown.scalaforimpatient.chapter2

object Exercise6 extends App {
  val s = "Hello"
  var sum = 1
  for (char <- s) sum *= char.toInt
  assert(825152896 == sum)
}