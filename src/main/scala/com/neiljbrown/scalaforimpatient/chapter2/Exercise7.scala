/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown 
 * Chapter 2. Control Structures and Functions, Exercise 7.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Solve the preceding exercise without writing a loop. (Hint: Look at the StringOps Scaladoc).
//
// A) One way to solve this is to use StringOps.map() to convert the char to its unicode value, and use 
// StringOps.product() on each value
package com.neiljbrown.scalaforimpatient.chapter2

object Exercise7 extends App {
  var s = "Hello"
  var sum = s.map(char => char.toInt).product
  assert(825152896 == sum)  
}