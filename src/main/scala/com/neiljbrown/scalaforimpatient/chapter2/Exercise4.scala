/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 2. Control Structures and Functions, Exercise 4.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write a Scala equivalent for the Java loop: for (int i = 10; i >= 0; i--) System.out.println(i);
// A) Use Scala's version of a for loop
package com.neiljbrown.scalaforimpatient.chapter2

object Exercise4 extends App {
  // Note: The for loop uses an expression which creates a Range (integer collection) with values 10 to 0 inclusive, 
  // subtracting by 1. There are a couple of ways to create this Range, all based on using the 'to' method:
  // 10 to 0 by -1
  // (0 to 10).reverse
  // 10 to(1,-1)
  for (i <- (10 to 0 by -1)) println(i)
}