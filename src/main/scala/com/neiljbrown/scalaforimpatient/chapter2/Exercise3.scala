/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 2. Control Structures and Functions, Exercise 3.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Come up with one situation where the assignment x = y = 1 is valid in Scala. (Hint: Pick a suitable type for x).
// A) The result of an assignment is always Unit, so x needs to be of type Unit.
package com.neiljbrown.scalaforimpatient.chapter2

object Exercise3 extends App {
  var y = 0
  var x: Unit = {}
  x = y = 1
}