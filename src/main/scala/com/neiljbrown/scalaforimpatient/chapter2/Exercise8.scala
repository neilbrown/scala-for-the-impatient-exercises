/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 2. Control Structures and Functions, Exercise 8.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write a function product(s: String) that computes the product, as described in the preceding exercises.
package com.neiljbrown.scalaforimpatient.chapter2

object Exercise8 extends App {
  def product(s: String): Long = {
    var sum = 1
    for (char <- s) sum *= char.toInt
    println("The product of the Unicode codes for letters of string [" + s + "] is [" + sum + "].")
    sum
  }

  product("Hello")
  assert(825152896 == product("Hello"))
}