/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 2. Control Structures and Functions, Exercise 5.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write a procedure countdown(n: Int) that prints the numbers from n to 0
package com.neiljbrown.scalaforimpatient.chapter2

object Exercise5 extends App {
  // Scala provides a shorthand way of declaring procedures (a function that returns no value) - the body is enclosed in 
  // braces without a preceding = symbol, then the return type is inferred to be Unit. 
  def countdown(n: Int) { // Note there is no = before the preceding brace for a procedure declaration
    println("Counting down from [" + n + "] to zero...")
    for (i <- (n to 0 by -1)) println(i)
  }

  countdown(5)
  countdown(-1)
}
