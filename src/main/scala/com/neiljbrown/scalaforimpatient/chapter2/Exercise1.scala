/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown 
 * Chapter 2. Control Structures and Functions, Exercise 1.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) The signum of a number is 1 if the number is positive, -1 if it is negative, and 0 if it is zero. Write a function 
// that computes this value.
package com.neiljbrown.scalaforimpatient.chapter2

object Exercise1 extends App {
  // Scala supports functions (a method which doesn't use an object instance) - like static methods in Java
  def signum(num: Int) = { 
    if (num > 0) 1 else if (num < 0) -1 else 0
  }

  assert(signum(2) == 1)
  assert(signum(1) == 1)
  assert(signum(0) == 0)
  assert(signum(-1) == -1)
  assert(signum(-2) == -1)
}