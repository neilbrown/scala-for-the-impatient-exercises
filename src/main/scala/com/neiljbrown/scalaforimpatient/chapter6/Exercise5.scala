/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 6. Objects, Exercise 5.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write a Scala application, using the App trait, that prints the command-line arguments in reverse order, 
// separated by spaces. For example, scala Reverse Hello World, should print "World Hello".
package com.neiljbrown.scalaforimpatient.chapter6

// In Scala the App trait provides a convenient way to create applications, avoiding the need to provide a main method.
object Exercise5 extends App {
  // Constructor code
  if (args.length > 0)
    println(args.reverse.mkString(" "))
  else
    println("Supply one or more words as arguments.")
}