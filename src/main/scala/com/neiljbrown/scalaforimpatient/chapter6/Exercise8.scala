/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 6. Objects, Exercise 8.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write an enumeration describing the eight corners of the RGB colour cube. As IDs, use the colour values (for 
// example, 0xff0000 for Red).
package com.neiljbrown.scalaforimpatient.chapter6

/**
 * An enumeration describing the eight corners of the RGB colour cube. The IDs of the enum members are the colour's
 * hex representation in a 24-bit (true colour) system, with Red on the x-axis, Blue on the y-axis, and Green on the
 * z-axis.
 *
 * @see http://en.wikipedia.org/wiki/RGB_color_model
 * @see http://en.wikipedia.org/wiki/List_of_monochrome_and_RGB_palettes
 */
object Rgb24BitColourCubeCorners extends Enumeration {
  // The Scala Enumeration class supports passing in your own ID values for each enum member
  val black = Value(0x000000) // zero intensity for all primary colours  	
  val red = Value(0xff0000) // primary colour red, modelled on the x-axis
  val blue = Value(0x00ff00) // primary colour blue, modelled on the y-axis  
  val green = Value(0x0000ff) // primary colour green, modelled on the z-axis
  val yellow = Value(0xff00ff) // seconday colour - red + green	  	    
  val cyan = Value(0x00ffff) // secondary colour - green + blue  	    
  val magenta = Value(0xffff00) // secondary colour - red + blue
  val white = Value(0xffffff) // full intensity for all primary colours
}

object Exercise8 extends App {
  assert(Rgb24BitColourCubeCorners.red.id == 0xff0000)
}