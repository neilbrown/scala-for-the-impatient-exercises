/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 6. Objects, Exercise 6.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write an enumeration describing the four playing card suites so that the toString method returns clubs, diamnonds, 
// hearts or spades.
package com.neiljbrown.scalaforimpatient.chapter6

// Scala doesn't have an enum type but provides the Enumeration helper class to create enumerations.
// Define an enumeration by declaring an object that extends the Enumeration class and a field for each required value, 
// initialised via a call to the Value method
object CardSuite extends Enumeration {
  // Each call to the Value method returns a new instance of an Enumeration inner class also called Value.
  // The Value method call generates an ID for each enum value, starting at zero and incrementing by one. The name of 
  // the enum member if not specified defaults to the field name.
  val clubs, diamonds, hearts, spades = Value
}

object Exercise6 extends App {
  assert(CardSuite.values.mkString(",") == "clubs,diamonds,hearts,spades")
}  