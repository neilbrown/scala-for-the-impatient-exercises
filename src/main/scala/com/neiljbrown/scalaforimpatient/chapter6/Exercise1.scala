/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 6. Objects, Exercise 1.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
//
// Q) Write an object Conversions with methods inchesToCentimeters, gallonsToLiters, and milesToKilometers.

// A Scala Object can serve a similar purpose to a utility class of static methods in Java (or Java 8 Default Methods)
package com.neiljbrown.scalaforimpatient.chapter6

object Conversions {
  def inchesToCentimeters(inches: Double) = {
    inches * 2.54
  }

  def gallonsToLiters(gallons: Double) = {
    gallons * 4.5609
  }

  def milesToKilometers(miles: Double) = {
    miles * 1.609344
  }
}

object Exercise1 extends App {
  assert(Conversions.inchesToCentimeters(2) == 5.08)
  assert(Conversions.gallonsToLiters(2) == 9.1218)
  assert(Conversions.milesToKilometers(2) == 3.218688)
}