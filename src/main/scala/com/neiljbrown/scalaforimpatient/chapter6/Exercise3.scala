/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 6. Objects, Exercise 3.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Define an Origin object that extends java.awt.Point. Why is this not actually a good idea? (Have a close look at 
// the methods of the Point class).

// A) This is not a good idea as Scala Objects are meant to support implementing singletons and typically you will want 
// to create many points, not one. Also, a Point is typically constructed from two co-ordinates, but Scala objects 
// do not support constructor arguments.
package com.neiljbrown.scalaforimpatient.chapter6

object Origin extends java.awt.Point {}

object Exercise3 extends App {
  // Scala objects don't suppot constructor arguments, so you can't construct them from two co-ordinates
  val origin = Origin
  origin.setLocation(10, 15)
}