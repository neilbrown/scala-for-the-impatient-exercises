/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 6. Objects, Exercise 7.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Implement a function that checks whether a card suite value from the preceding exercise is red.
package com.neiljbrown.scalaforimpatient.chapter6

// Scala doesn't have an enum type but provides the Enumeration helper class to create enumerations.
// Define an enumeration by declaring an object that extends the Enumeration class and a field for each required value, 
// initialised via a call to the Value method
object CardSuiteV2 extends Enumeration {
  // Each call to the Value method returns a new instance of an Enumeration inner class also called Value.
  // The Value method call generates an ID for each enum value, starting at zero and increment by one. The name of the
  // enum member if not specified defaults to the field name.
  val clubs, diamonds, hearts, spades = Value

  /**
   * Returns true if the supplied card suite is red.
   *
   * @param suite The card suite to check, a member of this enumeration. Note that because this enumeration is
   * implemented by extending Enumeration, the type of this enumeration is CardSuiteV2.Value and not CardSuiteV2 (which 
   * is just the type of the object holding the values). A type alias could be used in the object declaration but for
   * transparency has not been in this example.
   */
  def isRed(suite: CardSuiteV2.Value) = {
    // Would have preferred to have adopted the more object-oriented approach of declaring an additional property in the 
    // enumeration which defines the colour of the suit on construction, but Scala's Enumeration class does not support 
    // declaring additional fields. 
    (suite == diamonds || suite == hearts)
  }
}

object Exercise7 extends App {
  assert(!CardSuiteV2.isRed(CardSuiteV2.clubs))
  assert(CardSuiteV2.isRed(CardSuiteV2.diamonds))
  assert(CardSuiteV2.isRed(CardSuiteV2.hearts))
  assert(!CardSuiteV2.isRed(CardSuiteV2.spades))

  assert((CardSuiteV2.values filter CardSuiteV2.isRed).mkString(",") == "diamonds,hearts")
}