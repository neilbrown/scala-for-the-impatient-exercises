/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 6. Objects, Exercise 4.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Define a Point class with a companion object so that you can construct Point instances as Point(3,4) without
// using new. 

// Primary constructor supports construction from pair of co-ordinates which become immutable properties.
package com.neiljbrown.scalaforimpatient.chapter6

class Point(val x: Int, val y: Int) {}

// Companion object for Point class.
object Point {
  // Provides a convenient way of constructing new instances of the companion class, with needing the 'new' keyword
  def apply(x: Int, y: Int) =
    new Point(x, y)
}

object Exercise4 extends App {
  val p = Point(10, 15)
  assert(p.x == 10)
  assert(p.y == 15)
}