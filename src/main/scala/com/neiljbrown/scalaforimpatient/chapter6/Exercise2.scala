/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 6. Objects, Exercise 2.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) The preceding problem wasn't very object-oriented. Provide a general super-class UnitConversion and define 
// objects InchesToCentimeters, GallonsToLiters, and MilesToKilometers that extend it.

// Scala Objects can extend a class and/or one or more traits, just like a class
package com.neiljbrown.scalaforimpatient.chapter6

abstract class UnitConversion {
  def convert(toConvert: Double): Double
}

object InchesToCentimeters extends UnitConversion {
  override def convert(inches: Double) = {
    inches * 2.54
  }
}

object GallonsToLiters extends UnitConversion {
  override def convert(gallons: Double) = {
    gallons * 4.5609
  }
}

object MilesToKilometers extends UnitConversion {
  override def convert(miles: Double) = {
    miles * 1.609344
  }
}

object Exercise2 extends App {
  assert(InchesToCentimeters.convert(2) == 5.08)
  assert(GallonsToLiters.convert(2) == 9.1218)
  assert(MilesToKilometers.convert(2) == 3.218688)
}