/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 5. Classes, Exercise 7.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write a class Person with a primary constructor that accepts a string containing a first name, a space, and a 
// last name, such as new Person("Fred Smith"). Supply read-only properties firstName and lastName. Should the primary 
// constructor be a var, a val, or a plain paremeter? Why?
package com.neiljbrown.scalaforimpatient.chapter5

// Primary constructor - Specifies a plain (not var or val) param as we don't want to store it as a field
class PersonV2(name: String) {

  // Scala generates a read-only property (getter only) for public val fields
  val firstName: String = name.split("\\s+")(0)
  val lastName: String = name.split("\\s+")(1)
}

object Exercise7 extends App {
  val p1 = new PersonV2("Neil Brown")
  assert(p1.firstName == "Neil")
  assert(p1.lastName == "Brown")
}