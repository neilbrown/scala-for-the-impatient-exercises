/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 5. Classes, Exercise 2.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write a class BankAccount with methods deposit and withdraw, and a read-only property balance.
package com.neiljbrown.scalaforimpatient.chapter5

import scala.math.BigDecimal.double2bigDecimal
import scala.math.BigDecimal.int2bigDecimal

// Scala classes are public by default
class BankAccount {

  // If you want a read-only property for a mutable field (i.e. a field whose value can be changed by the class but not 
  // client code) then you need to declare the field as private, and provide your own getter
  // Note - Using BigDecimal type for currency which is best practice in Java - not sure if also the case for Scala?
  private var balance: BigDecimal = 0.0

  // Custom getter - You need to provide your own getter method for a retrieving a private field
  // This is an example of a parameterless method
  def getBalance = balance

  // Deposits funds and returns new balance
  def deposit(funds: BigDecimal) {
    if (funds > 0) balance += funds
    balance
  }

  // Withdraws funds and returns new balance
  def withdraw(funds: BigDecimal) {
    if (funds <= balance) {
      balance -= funds
    }
    balance
  }
}

object Exercise2 extends App {
  val a = new BankAccount
  a.deposit(100.00)
  a.withdraw(50.00)
  a.withdraw(25.00)
  assert(a.getBalance == 25.00)
}