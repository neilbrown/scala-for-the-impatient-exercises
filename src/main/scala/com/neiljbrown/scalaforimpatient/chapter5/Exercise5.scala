/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 5. Classes, Exercise 5.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Make a class Student with read-write JavaBeans properties 'name' (of type String) and 'id' (of type Long). What 
// methods are generated? (Use javap to check). Can you call the JavaBeans getters and setters in Scala? Should you?
//
// A) Scala generates the default scala accessor methods for the fields - id() and name() - as well as the JavaBean
// versions, as should by the output from javap:
// $ javap Student
// Compiled from "chapter5Exercise5.scala"
// public class Student extends java.lang.Object{
//    public java.lang.String name();
//    public long id();
//    public java.lang.String getName();
//    public long getId();
//    public Student(java.lang.String, long);
// }
package com.neiljbrown.scalaforimpatient.chapter5

// Annotate Scala fields @BeanProperty to generate getter and setter methods which conform to the Java Bean spec.
import scala.beans.BeanProperty

// Declare primary construtor params as (public) val, so that Scala auto-generates public properties
class Student(@BeanProperty val name: String, @BeanProperty val id: Long) {
}

object Exercise5 extends App {
  val s1 = new Student("Neil", 1)
  assert("Neil" == s1.getName)
  assert(1 == s1.getId)
}