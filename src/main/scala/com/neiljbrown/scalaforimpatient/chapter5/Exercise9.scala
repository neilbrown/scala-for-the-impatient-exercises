/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 5. Classes, Exercise 9.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Reimplement the class of the preceeding exercise in Java, C#, or C++ (your choice). How much shorter is the 
// Scala class?
//
// A) In Java, see CarV2.java, it's 23 lines (excluding whitespace). In Scala (see previous exercise) it's 8 lines.
package com.neiljbrown.scalaforimpatient.chapter5

object Exercise9 extends App {
  val car = new CarV2("Ford", "Escort RS", 1979, "rally1")
}