/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 5. Classes, Exercise 4.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Reimplement the Time class from the preceding exercise so that the internal representation is the number of 
// minutes since midnight (between 0 and 24 x 60 - 1). Do not change the public interface. That is, client code should 
// be unaffected by your change. 
package com.neiljbrown.scalaforimpatient.chapter5

// Drop use of val in declaration of primary constructor class params so that Scala just treats them as regular method
// params and does not create fields. 
// Note - Also had to rename these params (hours --> hrs, and minutes --> mins) because although they're not val/var 
// Scala still creates property accessor methods for them because (I think...) they're used in the primary constructor, 
// and then complains that they clash with the (now custom) accessor methods hours and minutes
class TimeV2(hrs: Int, mins: Int) {

  // Field storing time as no. of minutes since midnight - private so that auto-generated getter/setter also private
  private var numMinsSinceMidnight: Int = 0

  // Constructor code
  require(hrs > 0 && hrs < 24, "Hours must be between 0 and 23 inclusive")
  require(mins > 0 && mins < 60, "Minutes must be between 0 and 59 inclusive")

  // Convert and store internal representation of time
  numMinsSinceMidnight = (hrs * 60) + mins

  def before(other: TimeV2): Boolean = {
    // Re-implemented to account for change to internal representation of time
    return this.numMinsSinceMidnight < other.numMinsSinceMidnight
  }

  // This is now a custom (rather than generated) accessor method, maintaining the original public interface
  def hours = numMinsSinceMidnight / 60

  // This is now a custom (rather than generated) accessor method, maintaining the original public interface
  def minutes = numMinsSinceMidnight % 60
}

object Exercise4 extends App {
  // Following client code is unchanged from the previous exercise - proof that the public interface hasn't changed
  val t1 = new TimeV2(21, 45)
  assert(t1.hours == 21)
  assert(t1.minutes == 45)
  val t2 = new TimeV2(21, 46)
  val t3 = new TimeV2(21, 46)

  assert(t1.before(t2))
  assert(t2.before(t1) == false)
  assert(t2.before(t3) == false)
  assert(t3.before(t2) == false)
}