/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown 
 * Chapter 5. Classes, Exercise 1.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Improve the Counter class in Section 5.1, "Simple Classes and Paramterless Methods", on page 51 so that it doesn't
// turn negative at Int.MaxValue.
//
// A) Declare the type of the field as BigInt.
package com.neiljbrown.scalaforimpatient.chapter5

import scala.math.BigInt.int2bigInt

// Classes are public by default
class Counter {
  // Specify the type of field as BigInt, rather than the default of Int. BigInt supports arbitrary precision, meaning 
  // that it can represent all discrete numbers given enough memory is available.
  // Also dropped the private access modifier so we can use the property's public setter.
  var value: BigInt = 0 // You must initialize a field

  def increment() { value += 1 } // Methods are public by default
  def current() = value
}

object Exercise1 extends App {
  val myCounter = new Counter
  // Use public setter to initialise field to max int value plus 1
  myCounter.value = Int.MaxValue + 1
  // Use custom getter to prove the field has not overflowed
  assert(myCounter.current == Int.MaxValue + 1)
}