/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 5. Classes, Exercise 9.
 */
package com.neiljbrown.scalaforimpatient.chapter5;

/**
 * Java implementation of Car class. Answer to chapter 5 exercise 9.
 */
public class CarV2 {
  private static final int MODEL_YEAR_NOT_SET = -1;
  private static final String LICENSE_PLATE_NOT_SET = "";

  private String manufacturer;
  private String modelName;
  private int modelYear = MODEL_YEAR_NOT_SET;
  private String licensePlate = LICENSE_PLATE_NOT_SET;

  public CarV2(String manufacturer, String modelName) {
  	this(manufacturer, modelName, MODEL_YEAR_NOT_SET, LICENSE_PLATE_NOT_SET);
  }

  public CarV2(String manufacturer, String modelName, Integer modelYear) {
  	this(manufacturer, modelName, modelYear, LICENSE_PLATE_NOT_SET);  	
  }

  public CarV2(String manufacturer, String modelName, String licensePlate) {
  	this(manufacturer, modelName, MODEL_YEAR_NOT_SET, licensePlate);  	  	
  }  

  public CarV2(String manufacturer, String modelName, Integer modelYear, String licensePlate) {
  	this.manufacturer = manufacturer;
  	this.modelName = modelName;  	
  	this.modelYear = modelYear != null ? modelYear : MODEL_YEAR_NOT_SET;
  	this.licensePlate = licensePlate != null ? licensePlate : LICENSE_PLATE_NOT_SET;
  }    
}