/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 5. Classes, Exercise 3.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write a class Time with read-only properties hours and minutes and a method before(other: Time): Boolean that 
// checks whether this time comes before the other. A Time object should be constructed as new Time(hrs, min), where
// hr is in military time format (between 0 and 23).
package com.neiljbrown.scalaforimpatient.chapter5

// Parameters of the primary constructor are placed after the class name, and these are automaticalled turn into fields.
// If we assume the class is immutable and declare the primary constructor params as val, then Scala will automatically 
// create read-only properties (only generate a getter). (If instead we wanted to create read-only _mutable_ 
// property then the params would need to be delcared private, and a custom getter method provided).
class Time(val hours: Int, val minutes: Int) {

  // Constructor code
  require(hours > 0 && hours < 24, "Hours must be between 0 and 23 inclusive")
  require(minutes > 0 && minutes < 60, "Minutes must be between 0 and 59 inclusive")

  def before(other: Time): Boolean = {
    return this.hours < other.hours || (this.hours == other.hours && this.minutes < other.minutes)
  }
}

object Exercise3 extends App {
  val t1 = new Time(21, 45)
  // Use Scala's auto-generated accessor method for hours property
  assert(t1.hours == 21)
  // Use Scala's auto-generated accessor method for minutes property
  assert(t1.minutes == 45)
  val t2 = new Time(21, 46)
  val t3 = new Time(21, 46)

  assert(t1.before(t2))
  assert(t2.before(t1) == false)
  assert(t2.before(t3) == false)
  assert(t3.before(t2) == false)
}