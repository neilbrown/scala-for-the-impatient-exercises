/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 5. Classes, Exercise 6.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) In the Person class of Section 5.1, "Simple Classes and Parameterless Methods" on page 51, provide a primary
// consturctor that turns negative ages to 0.
package com.neiljbrown.scalaforimpatient.chapter5

class Person(var age: Int) {
  // Constructor code
  if (age < 0) age = 0
}

object Exercise6 extends App {
  val p1 = new Person(-1)
  assert(p1.age == 0)
}