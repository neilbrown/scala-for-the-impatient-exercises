/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 5. Classes, Exercise 8.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Make a class car with read-only properties for manufacturer, model name, and model year, and a read-write 
// property for the license plate. Supply four constructors. All require the manufacturer and model name. Optionally, 
// model year and license plate can also be specified in the constructor. If not, the model year is set to -1 and the 
// license plate to the empty string. Which constructor are you choosing as the primary constructor? Why?
//
// A) It is possible to support 4 ways of constructing a Car by writing 3 consturctor methods, with the primary method
// used to declare the 4 properties, and specify the default values for the 2 optional properties -
package com.neiljbrown.scalaforimpatient.chapter5

class Car(val manufacturer: String, val modelName: String, val modelYear: Int = -1, var licensePlate: String = "") {

  def this(manufacturer: String, modelName: String, modelYear: Int) {
    this(manufacturer, modelName, modelYear, "")
  }

  def this(manufacturer: String, modelName: String, licensePlate: String) {
    this(manufacturer, modelName, -1, licensePlate)
  }
}

object Exercise8 extends App {
  // Two (mandatory) arg ctor
  val c1 = new Car("Ford", "Escort")
  // Check read-only property accessors
  assert(c1.manufacturer == "Ford")
  assert(c1.modelName == "Escort")
  // Check defaults
  assert(c1.modelYear == -1)
  assert(c1.licensePlate == "")

  // Three arg ctor, with default license plate
  val c2 = new Car("Ford", "Escort", 1973)
  assert(c2.modelYear == 1973)
  assert(c2.licensePlate == "")

  // Three arg ctor, with default model year
  val c3 = new Car("Ford", "Escort", "L1C PLAT3")
  // Check default modelYear when license plate is supplied
  assert(c3.modelYear == -1)
  assert(c3.licensePlate == "L1C PLAT3")

  // Full, 4 arg ctor
  val c4 = new Car("Ford", "Escort", 1973, "L1C PLAT3")
  assert(c4.manufacturer == "Ford")
  assert(c4.modelName == "Escort")
  assert(c4.modelYear == 1973)
  assert(c4.licensePlate == "L1C PLAT3")
}