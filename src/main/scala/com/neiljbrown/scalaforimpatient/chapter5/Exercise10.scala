/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 5. Classes, Exercise 10.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Consider the class
//
// class Employee(val name: String, var salary: Double) {
//   def this() { this ("John Q. Public", 0.0) }
// }
//
// Rewrite it to use explicit fields and a default primary constructor. Which form do you prefer? Why?
//
// A) Prefer the original example above (with mult-arg primary constructor) as it allows name field to be immutable and
// (less importantly) is more concise.
package com.neiljbrown.scalaforimpatient.chapter5

class Employee {
  var name: String = "John Q. Public"
  var salary: Double = 0.0

  def this(name: String, salary: Double) {
    this()
    this.name = name
    this.salary = salary
  }
}

object Exercise10 extends App {
  val e1 = new Employee
  assert(e1.name == "John Q. Public")
  assert(e1.salary == 0.0)
}