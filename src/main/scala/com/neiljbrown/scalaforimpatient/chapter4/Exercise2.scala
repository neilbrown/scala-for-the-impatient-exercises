/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 4. Maps and Tuples, Exercise 2.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write a program that reads words from a file. Use a mutable map to count how often each word appears. To read the 
// words, simply use java.util.Scanner. Or look at Chapter 9 for a Scalaesque way. At the end, print out all words and 
// their counts.
package com.neiljbrown.scalaforimpatient.chapter4

object Exercise2 extends App {
  if (args.length < 1) {
    println("Filename required.")
    sys.exit
  }
  val filename = args(0)

  // A Java solution for counting the words in a file, using java.util.Scanner. (The regex specfies that comma 
  // whitespace and period whitespace should be used as a delimiter as well just whitespace)
  val in = new java.util.Scanner(new java.io.File(filename)).useDelimiter("\\s+|,\\s+|\\.\\s+")

  // Create a mutable map - one whose keys and values can be updated
  val wordsCount = new scala.collection.mutable.HashMap[String, Int]
  while (in.hasNext()) {
    val nextWord = in.next
    wordsCount(nextWord) = if (wordsCount.contains(nextWord)) wordsCount(nextWord) + 1 else 1
  }

  println("Word count for file [" + filename + "]\n" + wordsCount.mkString("\n"))
}