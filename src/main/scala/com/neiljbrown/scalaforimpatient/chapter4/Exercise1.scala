/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown 
 * Chapter 4. Maps and Tuples, Exercise 1.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Set up a map of prices for a number of gizmos you covet. Then produce a second map with the same keys and the 
// prices at a 10 percent discount.
package com.neiljbrown.scalaforimpatient.chapter4

object Exercise1 extends App {
  val gizmoPrices : Map[String, Double] = Map("gizmo1" -> 10, "gizmo2" -> 128, "gizmo3" -> 75)
  val discountGizmoPrices = for ( (gizmo, price) <- gizmoPrices ) yield (gizmo, price - (price * 10/100))

  assert(discountGizmoPrices == Map("gizmo1" -> 9.0, "gizmo2" -> 115.2, "gizmo3" -> 67.5))
}