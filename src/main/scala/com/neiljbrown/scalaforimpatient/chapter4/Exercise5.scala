/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown 
 * Chapter 4. Maps and Tuples, Exercise 5.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Repeat the preceding exercise with a java.util.TreeMap that you adapt to the Scala API.

// Import support for converting Java Map implementations to Scala maps
package com.neiljbrown.scalaforimpatient.chapter4

import scala.collection.JavaConversions.mapAsScalaMap

object Exercise5 extends App {
  if (args.length < 1) {
	  println("Filename required.")
	  sys.exit
  }
  val filename = args(0)

  // A Java solution for counting the words in a file, using java.util.Scanner
  val in = new java.util.Scanner(new java.io.File(filename)).useDelimiter("\\s+|,\\s+|\\.\\s+")

  // By default Scala creates hash table implementations of maps. For immutable maps, Scala provides the SortedMap. As 
  // of Scala 2.9.2, Scala does not provide a mutable sorted map. You have to use a Java TreeMap instead -
  var wordsCount: scala.collection.mutable.Map[String, Int] = new java.util.TreeMap[String, Int]
  while (in.hasNext()) {
	  val nextWord = in.next
	  // You can’t update an immutable map, but you can instead obtain a new map with the desired updates -
	  wordsCount = wordsCount + (nextWord -> (if (wordsCount.contains(nextWord)) wordsCount(nextWord) + 1 else 1))
  }

  println("Word count for file [" + filename + "]\n" + wordsCount.mkString("\n"))
}