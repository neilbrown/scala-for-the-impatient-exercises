/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown 
 * Chapter 4. Maps and Tuples, Exercise 7.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Print a table of all Java properties, like this (i.e. with values justified based on length of longest key)
// java.runtime.name                | Java(TM) SE Runtime Environment
// sun.boot.library.path            | /home/apps/....
// ....
// You need to find the length of the longest key before you can print the table.
package com.neiljbrown.scalaforimpatient.chapter4

object Exercise7 extends App {
  // It's possible to convert from a java.util.Properites implementation of a java.util.Hashtable to a Scala 
  // Map[String, String] using a specific JavaConversions class -
  import scala.collection.JavaConversions.propertiesAsScalaMap
  val props: scala.collection.Map[String, String] = System.getProperties()

  // Use functional approach of using Set.reduceLeft() to find the longest key instead of a loop
  val longestKey = props.keySet.reduceLeft((k1, k2) => if (k1.length > k2.length) k1 else k2)

  for ((k, v) <- props) println(k + " " * (longestKey.length - k.length) + "| " + v + "\r")
}