/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown 
 * Chapter 4. Maps and Tuples, Exercise 4.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Repeat the preceding exercise with a sorted map, so that the words are printed in sorted order.
package com.neiljbrown.scalaforimpatient.chapter4

object Exercise4 extends App {
  if (args.length < 1) {
	  println("Filename required.")
	  sys.exit
  }
  val filename = args(0)

  // A Java solution for counting the words in a file, using java.util.Scanner. (The regex specfies that comma 
  // whitespace and period whitespace should be used as a delimiter as well just whitespace)
  val in = new java.util.Scanner(new java.io.File(filename)).useDelimiter("\\s+|,\\s+|\\.\\s+")

  // By default Scala creates hash table implementations of maps. If you need to visit the keys in sorted order Scala
  // provides an immutable SortedMap
  var wordsCount = scala.collection.immutable.SortedMap[String, Int]()
  while (in.hasNext()) {
    val nextWord = in.next
    // You can’t update an immutable map, but you can instead obtain a new map with the desired updates -
    wordsCount = wordsCount + (nextWord -> (if (wordsCount.contains(nextWord)) wordsCount(nextWord) + 1 else 1))
  }

  println("Word count for file [" + filename + "]\n" + wordsCount.mkString("\n"))
}