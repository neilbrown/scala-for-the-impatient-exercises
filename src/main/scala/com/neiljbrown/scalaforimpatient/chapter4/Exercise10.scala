/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown 
 * Chapter 4. Maps and Tuples, Exercise 10.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) What happens when you zip together two strings, such as "Hello".zip("World")? Come up with a plausible use case.
package com.neiljbrown.scalaforimpatient.chapter4

object Exercise10 extends App {
  // A) A collection of pairs are returned containing the characters from each string which are at the same index
  assert("Hello".zip("World") == Vector(('H', 'W'), ('e', 'o'), ('l', 'r'), ('l', 'l'), ('o', 'd')))
}