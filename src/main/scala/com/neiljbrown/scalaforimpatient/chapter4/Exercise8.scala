/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown 
 * Chapter 4. Maps and Tuples, Exercise 8.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write a function minmax(values: Array[Int]) that returns a pair containing the smallest and largest values in the
// array.
package com.neiljbrown.scalaforimpatient.chapter4

object Exercise8 extends App {

  def minmax(values: Array[Int]) = {
    // Array.min and Array.max functions can be used to find min and max values.
    // A pair (a tuple of two values) can be returned by using (a,b)
    (values.min, values.max)
  }

  val values = Array(7, 3, 9, 9, 6, 0, 1, 3, 2, 4, 7, 8, 12, -1)

  assert(minmax(values) == (-1, 12))
}