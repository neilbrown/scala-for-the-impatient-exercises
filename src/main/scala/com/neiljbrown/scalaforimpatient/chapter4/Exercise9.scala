/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 4. Maps and Tuples, Exercise 9.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write a function lteqgt(values: Array[Int], v: Int) that returns a triple containing the counts of values less 
// than v, equal to v, and greater than v.
package com.neiljbrown.scalaforimpatient.chapter4

object Exercise9 extends App {

  def lteqgt(values: Array[Int], v: Int) = {
    // Array.count() function can be used to count the no. of matching occurrences
    // Return a tuple containing the 3 values.
    (values.count(_ < v), values.count(_ == v), values.count(_ > v))
  }

  val values = Array(7, 3, 9, 9, 6, 0, 1, 3, 2, 4, 7, 8, 12, -1)

  assert(lteqgt(values, 9) == (11, 2, 1))
}