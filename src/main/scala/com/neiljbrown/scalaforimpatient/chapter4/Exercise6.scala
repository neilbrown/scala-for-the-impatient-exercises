/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown 
 * Chapter 4. Maps and Tuples, Exercise 6.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Define a linked hash map that maps "Monday" to java.util.Calendar.MONDAY, and similarly for the other weekdays. 
// Demonstrate that the elements are visited in insertion order.
package com.neiljbrown.scalaforimpatient.chapter4

object Exercise6 extends App {
  // A Scala LinkedHashMap is a Map in which the entries are visited in insertion order
  val daysMap = scala.collection.mutable.LinkedHashMap(
    "Monday" -> java.util.Calendar.MONDAY,
    "Tuesday" -> java.util.Calendar.TUESDAY,
    "Wednesday" -> java.util.Calendar.WEDNESDAY,
    "Thursday" -> java.util.Calendar.THURSDAY,
    "Friday" -> java.util.Calendar.FRIDAY,
    "Saturday" -> java.util.Calendar.SATURDAY,
    "Sunday" -> java.util.Calendar.SUNDAY)

  // Enumerate through the Map and create a vector of the keys in the order they're returned
  val dayKeys = for ((k, v) <- daysMap) yield k

  // Prove the elements were visited in insertion order
  println("Visited map elements in key order [" + dayKeys.mkString(",") + "].")
  assert(dayKeys == Vector("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"))
}