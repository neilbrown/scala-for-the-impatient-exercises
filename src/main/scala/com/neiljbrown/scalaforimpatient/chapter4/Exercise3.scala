/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown 
 * Chapter 4. Maps and Tuples, Exercise 3.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Repeat the preceding exercise wtih an immutable map.
package com.neiljbrown.scalaforimpatient.chapter4

object Exercise3 extends App {
  if (args.length < 1) {
	  println("Filename required.")
	  sys.exit
  }
  val filename = args(0)

  // A Java solution for counting the words in a file, using java.util.Scanner. (The regex specfies that comma whitespace 
  // and period whitespace should be used as a delimiter as well just whitespace)
  val in = new java.util.Scanner(new java.io.File(filename)).useDelimiter("\\s+|,\\s+|\\.\\s+")

  // Creates an immutable Map[String, Int] whose contents can NOT be changed
  // No need for ‘new’; Implementation is inferred as HashMap; 
  var wordsCount = Map[String, Int]()
  while (in.hasNext()) {
    val nextWord = in.next
    // You can’t update an immutable map, but you can instead obtain a new map with the desired updates -
    wordsCount = wordsCount + (nextWord -> (if (wordsCount.contains(nextWord)) wordsCount(nextWord) + 1 else 1))
  }

  println("Word count for file [" + filename + "]\n" + wordsCount.mkString("\n"))
}