/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 8. Inheritance, Exercise 8.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Compile the Person and SecretAgent classes in Section 8.6, "Overriding Fields", on page 91, and analyze the class 
// files with javap. How many name fields are there? How many getter methods are there? What do they get? (Hint: Use the 
// -c and -private options).
package com.neiljbrown.scalaforimpatient.chapter8

class Person(val name: String) {
  override def toString = getClass.getName + "[name=" + name + "]"
}

class SecretAgent(codename: String) extends Person(codename) {
  // Overrides the name field in the superclass with a field as a means of concealing the actual name of the agent - 
  // there are better ways to achieve this, by making the field private in the subclass. 
  override val name = "secret"
  // Override the toString() method in the superclass with a field to also keep the name field private
  override val toString = "secret"
}

// scalac chapter8Exercise8.scala
// javap -c -private -classpath ./ SecretAgent
/* 
Compiled from "chapter8Exercise8.scala"
public class SecretAgent extends Person{
private final java.lang.String name;
private final java.lang.String toString;

public java.lang.String name();
  Code:
  ...

public java.lang.String toString();
  Code:
  ...

public SecretAgent(java.lang.String);
  Code:
  ...
}
*/

// Q) How many name fields are there?
// A) The name field is duplicated in the SecretAgent subclass. 

// Q) How many getter methods are there? What do they get?
// A) There are still getter methods for both the name and toString fields in the SecretAgent subclass. They're 
// still auto-generated as fields without an accessor modifier are public by default.
// They get the concealing fields in the subclass, rather than the fields in the superclass, as proved by the app below.

object Exercise8 extends App {
  val sa1 = new SecretAgent("007")
  assert(sa1.name == "secret")
  assert(sa1.toString == "secret")
}