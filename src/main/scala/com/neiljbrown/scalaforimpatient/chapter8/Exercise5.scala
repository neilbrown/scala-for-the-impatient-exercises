/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 8. Inheritance, Exercise 5.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Design a class Point whose x and y coordinate values can be provided in a constructor. Provide a subclass 
// LabeledPoint whose constructor takes a label value and x and y coordinates, such as new 
// LabeledPoint("Black Thursday", 1929, 230.07).
package com.neiljbrown.scalaforimpatient.chapter8

class Point(val x: Double, val y: Double)

// Note - When subclassing Point, the val (immutable) fields in the superclass have to be declared as being overridden, 
// which comes as a surprise to a newbie, but is a reflection of the fact that a field in Scala is a private (val) field 
// _and_ its accessor.
class LabeledPoint(val label: String, override val x: Double, override val y: Double) extends Point(x, y)

/* The more verbose Java equivalent of above 2 lines in Scala is - 
public class Point {
  private double x, y = 0.0;	
  public Point(double x, double y) {
    this.x = x;
    this.y = y;
  }
  public double getX() { return x; }
  public double getY() { return y; }
}
public class LabeledPoint extends Point {
  private String label = "";
  public LabeledPoint(String label, double x, double y) {
  	super(x, y);
  	this.label = label;
  }
}
*/

object Exercise5 extends App {
  val label = "Black Thursday"
  val x: Double = 1929
  val y = 230.07
  val lp1 = new LabeledPoint(label, x, y)
  assert(lp1.label == label)
  assert(lp1.x == x)
  assert(lp1.y == y)
}