/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 8. Inheritance, Exercise 9.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) In the Creature class of Section 8.10, "Construction Order and Early Definitions", on page 94, replace val range 
// with a def. What happens when you also use a def in the Ant subclass? What happens when you use a val in the 
// subclass? Why?
//
// A) When Create.range is a val and the Ant subclass overrides it with a val, as per page 94, then the size/length of 
// the env array in the subclass is zero, rather than 2 (the value of range in the subclass is ignored).
// As explained in the book, this is because the Ant constructor calls the Creature constructor before doing its own 
// construction, and the Create constructor, in order to init the env array, calls the range() getter. That method 
// returns the range field of the Ant class - but it has yet to be initialsed - is zero, rather than 2.
// As stated in the book - "The moral is that you should not rely on the value of a val in the body of a constructor"
//
// When Create.range is changed from an immutable field (val) to a method, it does not make any difference - the Ant.env 
// array is still zero length.
//
// If the Ant subclass is also then changed to override def Create.range with a method (def) instead, then this fixes 
// the problem - the Ant.env array length is now 2, as desired.  This must be because the overridden getter method IS
// initialised before the Ant class calls the Create constructor.
package com.neiljbrown.scalaforimpatient.chapter8

class Creature {
  def range: Int = 10
  val env: Array[Int] = new Array[Int](range)
}

class Ant extends Creature {
  override def range: Int = 2
}

object Exercise9 extends App {
  val c1 = new Creature
  // A Creature's env array size is always 10 regardless of what changes are made in this exercise
  assert(c1.env.size == 10)

  val a1 = new Ant
  // An Ant's env array size is only initialised to 2 if range is declared as a method in _both_ the Create superclass 
  // and the Ant subclass, otherwise it is zero.
  assert(a1.env.size == 2)
}