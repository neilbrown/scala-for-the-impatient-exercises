/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 8. Inheritance, Exercise 10.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) The file scala/collection/immutable/Stack.scala contains the definition:
//  class Stack[A] protected (protected val elems: List[A])
// Explain the meanings of the protected keywords. (Hint: Review the discussion of private constructors in Chapter 5).
//
// A) The first protected keyword declares the primary constructor as protected. In scala, this means the method is only 
// accessible in subclasses. (Unlike Java, in which a protected method is also accessible to classes in the same 
// package).
//
// The second protected keyword declares the immutable (val) field elems as protected. In scala, this means the field 
// is only accessible in subclasses. (Unlike Java, in which a protected field is also accessible to classes in the same 
// package).