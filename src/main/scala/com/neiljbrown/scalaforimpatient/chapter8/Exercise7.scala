/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 8. Inheritance, Exercise 7.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Provide a Square class that extends java.awt.Rectangle and has three constructors: one that constructs a square 
// with a given corner point and width, one that constructs a square with corner (0, 0) and a given width, and one 
// that constructs a sqaure with corner (0,0) and width 0.
package com.neiljbrown.scalaforimpatient.chapter8

/**
 * A geometric square defined in terms of its top-left coordinates and width.
 * <p>
 * Subclasses {@link java.awt.Rectangle}. Consequently the coordinates and width are specified as 32-bit integers (even
 * though the field's accessor methods return a double...).
 *
 * @param x The x coordinate of the top-left of the square.
 * @param y The y coordinate of the top-left of the square.
 * @param width The width of the square.
 */
// If a Scala class extends a Java class then its primary ctor must invoke one of the ctors in the Java superlcass using 
// the Scala class constructor syntax.
//
// In this case, the default constructor does not override the fields or accessors in the Rectangle superclass. It is 
// prevented from doing so because the Rectangle contructor uses params of type int, whereas its accessor methods 
// use type double.
class Square(x: Int, y: Int, width: Int) extends java.awt.Rectangle(x, y, width, width) {
  /** Auxillary ctor. Constructs a square with default top-left corner (0,0) and a given width. */
  def this(width: Int) {
    this(0, 0, width)
  }

  /** Auxillary ctor. Constructs an 'invisible' square with default top-left corner (0,0) and a default width of zero.*/
  def this() {
    this(0)
  }
}

object Exercise7 extends App {
  val s1 = new Square(1, 1, 2)
  assert(s1.x == 1.0)
  assert(s1.y == 1.0)
  assert(s1.width == 2.0)
  val s2 = new Square(3)
  assert(s2.x == 0.0)
  assert(s2.y == 0.0)
  assert(s2.width == 3.0)
  val s3 = new Square
  assert(s3.x == 0.0)
  assert(s3.y == 0.0)
  assert(s3.width == 0.0)
}