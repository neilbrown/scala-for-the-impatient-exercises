/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 8. Inheritance, Exercise 4.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Define an abstract class Item with methods price and description. A SimpleItem is an item whose price and 
// description are specified in the constructor. Take advantage of the fact that a val can override a def. A Bundle 
// is an item that contains other items. Its price is the sum of the prices in the bundle. Also provide a mechanism for 
// adding items to the bundle and a suitable description method.
package com.neiljbrown.scalaforimpatient.chapter8

import scala.collection.mutable.ArrayBuffer

/**
 * An item, which has a price and a description.
 */
// A class with one or more abstract methods must be declared as such with an abstract keyword
abstract class Item {
  // Abstract methods don't need an abstract keyword, they just have no method body
  def price: Double
  def description: String
}

/**
 * SimpleItem - an item where the price and description is supplied via the default ctor.
 */
// val (immutable) fields (in this case specified in the ctor) can override parameterless (only) abstract methods - 
// the field’s associated abstract getter / setter methods in superclass are overridden/implemented.
class SimpleItem(override val price: Double, override val description: String) extends Item {}

/**
 * An item that contains other items.
 */
class Bundle(var items: ArrayBuffer[Item]) extends Item {

  /** Auxiliary constructor for creating an empty Bundle. */
  def this() {
    this(new ArrayBuffer[Item]())
  }

  /**
   * @return The sum of the prices of all the items in this Bundle.
   */
  // When overriding methods declared as abstract the override keyword is not mandatory, but seems clearer.
  override def price: Double = {
    // The imperative way - 
    //val itemPrices = for (item <- this.items) yield item.price
    //itemPrices.sum
    // The functional way -
    this.items.map(_.price).sum
  }

  /**
   * @return A generic description detailing the no. of items in the bundle.
   */
  // When overriding methods declared as abstract the override keyword is not mandatory, but seems clearer.
  override def description: String = "Bundle of [" + this.items.size + "] items"

  /**
   * Adds a supplied item to this Bundle.
   *
   * @return this bundle, to support chained calls for adding more items.
   */
  def addItem(item: Item): Bundle = {
    this.items += item
    this
  }
}

object Exercise4 extends App {
  val si1 = new SimpleItem(1.0, "Item 1")
  val si2 = new SimpleItem(2.0, "Item 2")
  val b1 = new Bundle()
  b1.addItem(si1).addItem(si2)
  assert(b1.price == si1.price + si2.price)
  assert(b1.description == "Bundle of [2] items")
}