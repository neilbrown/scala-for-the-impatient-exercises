/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 8. Inheritance, Exercise 1.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Extend the following BankAccount class to a CheckingAccount class that charges $1 for every deposit and withdrawal.
package com.neiljbrown.scalaforimpatient.chapter8

class BankAccount(initialBalance: Double) {
  // Need to change field access modifier from private to protected to allow it to be accessed in subclass
  protected var balance = initialBalance
  def deposit(amount: Double) = { balance += amount; balance }
  def withdraw(amount: Double) = { balance -= amount; balance }
}

// Classes in Scala are subclassed (extended) in the same way as in Java.
// A subclass' primary ctor must invoke one of the ctors in superlcass using the Scala class constructor syntax
class CheckingAccount(iniitalBalance: Double) extends BankAccount(iniitalBalance) {
  final val TRANSACTION_CHARGE: Double = 1.0

  // You must use the override modifier when you override a method that isn’t abstract
  override def deposit(amount: Double) = {
    // A method in the superclass is invoked in the same way as in Java
    super.deposit(amount)
    chargeForTransaction
    balance
  }

  override def withdraw(amount: Double) = {
    super.withdraw(amount)
    chargeForTransaction
    balance
  }

  private def chargeForTransaction = { balance -= TRANSACTION_CHARGE }
}

object chapter8Exercise1 extends App {
  val ca = new CheckingAccount(10.00)
  assert(ca.deposit(5.00) == 14.00)
  assert(ca.withdraw(10.00) == 3.00)
}