/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 8. Inheritance, Exercise 3.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Consult your favourite Java or C++ textbook that is sure to have an example of a toy inheritance hierarchy, 
// perhaps involving employess, pets, graphicapackage com.neiljbrown.scalaforimpatient.chapter8l shapes or the link. 
// Implement the example in Scala.
package com.neiljbrown.scalaforimpatient.chapter8

/** 
 * A Person has a first name and a last name.
 */
class PersonV2(firstName: String, lastName: String) {

  /**
   * Overrides default toString() to print public field values.
   */
  // In Scala, you must use the override modifier when you override a method that isn’t abstract,
  override def toString = getClass.getName + ": firstName [" + firstName + "] lastName [" + lastName + "]"
}

/**
 * An Employee - a class of Person, with an additional ID and salary.
 * <p>
 * In Scala are extended (subclassed) in the same way as in Java, using the extends keyword.
 */
class Employee(firstName: String, lastName: String, salary: Double) extends PersonV2(firstName, lastName) {
  val id: Int = Employee.nextId()

  /**
   * Overrides and supplements superclass' toString().
   */
  // Invoking a method in a superclass is implemented in the same way as in Java, using the super keyword.
  override def toString = super.toString + " id [" + id + "] salary [" + salary + "]"  
}

/** Employee companion object */
object Employee {
  private var lastId = 0
  /** Utility method to track and return the next, sequential, global Employee ID */
  private def nextId() = { this.lastId += 1; lastId } 
}

object Exercise3 extends App {
  val e1 = new Employee("Neil", "Brown", 100000.00)
  assert(e1.id == 1)
  assert(e1.toString == "Employee: firstName [Neil] lastName [Brown] id [1] salary [100000.0]")
}