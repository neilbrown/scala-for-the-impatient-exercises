/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 8. Inheritance, Exercise 2.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Extend the BankAccount class of the preceding exercise into a class SavingsAccount that earns interest every month 
// (when a method earnMonthlyInterest is called) and has three free deposits or withdrawals every month. Reset the 
// transaction count in the earnMonthlyInterest method.
package com.neiljbrown.scalaforimpatient.chapter8

class BankAccountV2(initialBalance: Double) {
  // Need to change field access modifier from private to protected to allow it to be accessed in subclass
  protected var balance = initialBalance
  def deposit(amount: Double) = { balance += amount; balance }
  def withdraw(amount: Double) = { balance -= amount; balance }
  def currentBalance: Double = this.balance
}

// Define field for monthly interest rate by declaring it as a constructor argument
class SavingsAccount(monthlyInterestRate: Double, iniitalBalance: Double) extends BankAccountV2(iniitalBalance) {
  final val TRANSACTION_CHARGE: Double = 1.0
  // No. of transaction per month that are free of charge
  final val FREE_TRANSACTIONS_PER_MONTH: Int = 3
  // Count of no. of transactions in the last month
  private var monthlyTxnCount: Int = 0

  override def deposit(amount: Double) = {
    super.deposit(amount)
    processTransaction
    balance
  }

  override def withdraw(amount: Double) = {
    super.withdraw(amount)
    processTransaction
    balance
  }

  // Apply monthly interest and reset transaction count for month
  def earnMonthlyInterest = {
    this.balance += this.balance * (this.monthlyInterestRate / 100)
    this.monthlyTxnCount = 0
  }

  // Common processing to apply to all transactions - both deposits and withdrawals
  private def processTransaction = {
    this.monthlyTxnCount += 1
    chargeForTransaction
  }

  // Applies a charge for a transaction if the no. of transactions in the last month exceeds FREE_TRANSACTIONS_PER_MONTH
  private def chargeForTransaction = { if (this.monthlyTxnCount > FREE_TRANSACTIONS_PER_MONTH) balance -= TRANSACTION_CHARGE }
}

object Exercise2 extends App {
  val sa = new SavingsAccount(5.00, 10.00)
  // Check no charge on first 3 transactions
  assert(sa.deposit(5.00) == 15.00)
  assert(sa.withdraw(10.00) == 5.00)
  assert(sa.deposit(25.00) == 30.00)
  // Check charge applied to additional transactions
  assert(sa.withdraw(5.00) == 24.00)
  // End of month - apply interest, and reset transaction count
  sa.earnMonthlyInterest
  assert(sa.currentBalance == 25.20)
  // Check no charge applied on first transaction in next month
  assert(sa.deposit(1.00) == 26.20)
}