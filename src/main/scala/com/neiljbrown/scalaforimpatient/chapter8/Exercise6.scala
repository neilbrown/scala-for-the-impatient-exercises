/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 8. Inheritance, Exercise 6.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Define an abstract class Shape with an abstract method centerPoint and subclasses Rectangle and Circle. Provide 
// appropriate constructors for the subclasses and override the centerPoint method in each subclass.
package com.neiljbrown.scalaforimpatient.chapter8

/**
 * A 2-dimensional (x, y) co-ordinate.
 */
class PointV2(val x: Double, val y: Double) {
  // Override AnyRef.equals() to use value based equality
  final override def equals(other: Any) = {
    val that = other.asInstanceOf[PointV2]
    if (that == null) false
    else this.x == that.x && this.y == that.y
  }

  // AnyRef.hashCode() should be overriden when AnyRef.equals() is.
  final override def hashCode = this.x.hashCode + this.y.hashCode

  final override def toString: String = x + "," + y
}

abstract class Shape {
  /**
   * @return A PointV2 detailing the coordinates of the centre point of this Shape.
   */
  // Abstract methods don’t need an abstract keyword they just have no method body
  def centerPoint: PointV2
}

class Rectangle(val topLeft: PointV2, val width: Double, val height: Double) extends Shape {
  // When overriding methods declared as abstract the override keyword is not mandatory, but seems clearer.
  override def centerPoint: PointV2 = new PointV2(topLeft.x + 0.5 * width, topLeft.y + 0.5 * height)
}

class Circle(val origin: PointV2, val radius: Double) extends Shape {
  // When overriding methods declared as abstract the override keyword is not mandatory, but seems clearer.
  override def centerPoint: PointV2 = origin
}

object Exercise6 extends App {
  val p1 = new PointV2(2, 3)
  val c1 = new Circle(p1, 1.5)
  assert(c1.centerPoint == p1)
  val r1 = new Rectangle(p1, 8, 4)
  assert(r1.centerPoint == new PointV2(6.0, 5.0))
}