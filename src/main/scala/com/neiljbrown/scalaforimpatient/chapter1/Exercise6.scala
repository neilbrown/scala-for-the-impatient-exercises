/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 1. The Basics, Exercise 6.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Using BigInt, compute 2 to the power of 1024.
//
// A) A very large (300+ digit) number....
package com.neiljbrown.scalaforimpatient.chapter1

import scala.math.pow

object Exercise6 extends App {
  println("2 to the power of 1024 as a BigInt [" + BigInt(2).pow(1024) + "]")
}