/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown 
 * Chapter 1. The Basics, Exercise 1.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) In the Scala REPL (Read-Evaluate-Print-Loop), aka interpreter, type 3, followed by the Tab key. 
// What methods can be applied?
// A) Answer -
/*
%              &              *              +              -              /              >              >=             >>             >>>            ^              asInstanceOf   isInstanceOf   
toByte         toChar         toDouble       toFloat        toInt          toLong         toShort        toString       unary_+        unary_-        unary_~        |   
*/