/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 1. The Basics, Exercise 8.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) One way to create random file or directory names is to produce a random BigInt and convert it to base 36, 
// yielding a string such as "qsnvbevtomcj38o06kul". Poke around Scaladoc to find a way of doing this is Scala.

package com.neiljbrown.scalaforimpatient.chapter1

// Random companion (singleton) object requires import of util package
import util._

object Exercise8 extends App {
  println("Random BigInt in base36 [" + BigInt.apply(100, Random).toString(36) + "]")
}