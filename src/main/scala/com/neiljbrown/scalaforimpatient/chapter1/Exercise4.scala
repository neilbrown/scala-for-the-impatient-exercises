/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 1. The Basics, Exercise 4.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Scala lets you multiply a string with a number - try out 'crazy' * 3 in the REFL. What does this operation do?
// Where can you find it in Scaladoc
//
// A) The '*' operation on a string returns the current string concatenated 'n' times, e.g. "crazycrazycrazy"
// Scaladoc http://www.scala-lang.org/api/current/index.html#scala.collection.immutable.StringOps
package com.neiljbrown.scalaforimpatient.chapter1

object Exercise4 extends App {
  assert("crazy" * 3 == "crazycrazycrazy")
}