/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown 
 * Chapter 1. The Basics, Exercise 9.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) How do you get the first character of a string in Scala? The last character?
package com.neiljbrown.scalaforimpatient.chapter1

object Exercise9 extends App {
  // "astring"(0) is an abbrevation of String.apply(i: int)
  assert("hello world"(0) == 'h')
  assert("hello world".last == 'd')
}