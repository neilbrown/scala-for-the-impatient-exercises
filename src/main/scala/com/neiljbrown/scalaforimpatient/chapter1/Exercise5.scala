/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 1. The Basics, Exercise 5.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) What does 10 max 2 mean? In which class is the max method defined?
//
// A) Returns the maximum of the two methods. The max method is defined in RichInt. 
// See http://www.scala-lang.org/api/current/index.html#scala.runtime.RichInt
package com.neiljbrown.scalaforimpatient.chapter1

object Exercise5 extends App {
  assert((10 max 2) == 10)
}