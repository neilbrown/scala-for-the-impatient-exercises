/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown 
 * Chapter 1. The Basics, Exercise 2.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) In the Scala REPL, compute the square root of 3, and then square that value. By how much does the result differ
// from 3? (Hint: The res variables are your friend.)
package com.neiljbrown.scalaforimpatient.chapter1

object Exercise2 extends App {
  val d1 = math.sqrt(3)
  println("Square root of 3 is [" + d1 + "]")
  println("Square of the root of 3 is [" + math.pow(d1, 2) + "]")
}