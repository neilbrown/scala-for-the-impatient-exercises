/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 1. The Basics, Exercise 10.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) What do the take, drop, takeRight and dropRight string functions do? What advantage or disadvantage do they have 
// over using substring?
package com.neiljbrown.scalaforimpatient.chapter1

object Exercise10 extends App {
  // Ref. http://www.scala-lang.org/api/current/index.html#scala.collection.immutable.StringOps
  // StringOps.take(n:Int) - Selects the first 'n' elements. 
  assert("hello world".take(2) == "he")
  // StringOps.drop(n:Int) - Selects all but the first 'n' elements
  assert("hello world".drop(2) == "llo world")
  // StringOps.takeRight(n:Int) - Selects the last 'n' elements
  assert("hello world".takeRight(2) == "ld")
  // StringOps.dropRight(n:Int) - Selects all elements except the last 'n'
  assert("hello world".dropRight(2) == "hello wor")
}