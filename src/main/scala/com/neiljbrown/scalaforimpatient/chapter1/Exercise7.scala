/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 1. The Basics, Exercise 7.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) What do you need to import so that you can get a random prime as probablePrime(100, Random), without any 
// qualifiers before probablePrime and Random
package com.neiljbrown.scalaforimpatient.chapter1

// Random companion (singleton) object requires import of util package
import util._
// probablePrime() requires import of scala.BigInt
import scala.BigInt._

object Exercise7 extends App {
  println("probablePrime(100,Random) [" + probablePrime(100, Random) + "]")
}