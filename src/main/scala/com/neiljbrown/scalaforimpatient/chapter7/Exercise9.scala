/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 7. Packages and Imports, Exercise 1.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write a program that imports the java.lang.System class, reads the user name from the user.name system property, 
// reads a password from the Console object, and prints a message to the standard error stream if the password is not 
// "secret". Otherwise, print a greeting to the standard output stream. do not use any other imports, and do not use 
// any qualified names (with dots).
package com.neiljbrown.scalaforimpatient.chapter7

// Scala automatically imports java.lang._ so it's unnecessary to explicitly import java.lang.System

object Exercise9 extends App {
  val userName = System.getProperty("user.name")
  if (userName.trim.isEmpty) {
    System.err.println("A user name must be specified via Java system property user.name.")
    sys.exit(1)
  }
  println("Username: [" + userName + "]")

  // Scala automatically imports scala._ so its unnecessary to explicitly import scala.Console
  val password = "secret"
  while (Console.readLine("Password:") != password) {
    System.err.println("Invalid password. Please try again.")
  }
  println("Welcome!")
}