/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 7. Packages and Imports, Exercise 1.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write an example program to demonstrate that package com.horstmann.impatient is _not_ the same as package com 
// package horstmann package impatient.
//
// A) The question is too vague. I've assumed it is expecting you to show that a class declared using the nested package 
// syntax can access classes in the parent package, whereas classes declared using the chained package syntax cannot.
package com {
  package horstmann	{

  	class Person {}

    package impatient {

      // When a class is declared using the nested package syntax, classes in the parent packages are in scope/visible
      class Manager extends Person {}
    }
  }
}

// A package clause can contain a 'chain' of path segment
package com.horstmann.impatient {
  // When a class is declared using the chained package syntax, classes in the parent packages are NOT in scope/visible
  // NOTE: The following line therefore generates an expected compilation error 'not found: type Person'
  //class Employee extends Person {}	
}