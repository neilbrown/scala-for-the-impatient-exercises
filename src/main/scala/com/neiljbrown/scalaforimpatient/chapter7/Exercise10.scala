/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 7. Packages and Imports, Exercise 10.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Apart from StringBuilder, what other members of java.lang does the scala package override?
//
// A) Any java.lang class which has the same name as a class in the scala package or one of its child packages is 
// overridden.
//  
// Comparing the classes listed in http://docs.oracle.com/javase/6/docs/api/java/lang/package-frame.html 
// with http://www.scala-lang.org/api/current/index.html#scala.package, yields -
//
// Boolean
// Byte
// Double
// Float
// Long
// Short
// 
// Note - There are other classes defined in scala sub-packages, such as scala.collection.mutable.StringBuilder, 
// which will also override other java.lang classes.