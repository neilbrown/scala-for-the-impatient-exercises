/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 7. Packages and Imports, Exercise 7.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) In the preceding exercise, move all imports into the innermost scope possible.
//
// A) This exercise illustates that in Scala an import statement can be located anywhere in the source code, not just
// at the top of file as in Java
package com.neiljbrown.scalaforimpatient.chapter7

import java.util.{ HashMap => JavaHashMap }

object Exercise7 extends App {
  val scores = new JavaHashMap[String, Int]
  scores.put("Alice", 10)
  scores.put("Bob", 3)
  scores.put("Cindy", 8)

  import scala.collection.JavaConversions._
  // Import moved to here, immediately before the statement where it is needed
  import scala.collection.mutable.{ HashMap => MutableHashMap }
  val originalScores = new MutableHashMap[String, Int]
  originalScores.putAll(scores)

  assert(originalScores == Map("Alice" -> 10, "Bob" -> 3, "Cindy" -> 8))
}