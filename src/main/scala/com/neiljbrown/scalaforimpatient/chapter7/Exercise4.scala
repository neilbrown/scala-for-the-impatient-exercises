/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 7. Packages and Imports, Exercise 4.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Why do you think the Scala language designers provided the package object syntax instead of simply letting you
// add functions and variables to a package?
//
// A) It provides a standard convention of defining static methods (utilities) and fields (constants) that are available 
// to all classes in the same package. Plus, the JVM does not currently support defining standalone functions and 
// variables in a package.