/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 7. Packages and Imports, Exercise 8.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) What is the effect of
//   import java._
//   import javax._
// Is this a good idea?
//
// A) These statements import all the members in the java and javax packages, equivalent to Java import java.* and 
// import javax.*.  It's not a good idea as there are no classes in these packages and in Java at least import java.*, 
// does NOT also import subpackages. Regardless, global imports such as these could cause potential naming conflicts. 
// Finally, it's not necessary given that java.lang is imported by default in Java and Scala.