/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 7. Packages and Imports, Exercise 5.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) What is the meaning of: private[com] def giveRaise(rate: Double) ? It is useful?
//
// A) Declares a private method 'getRaise' which, via the use of [com] qualifier, is additionally available to 
// all classes in the (typically top-level) 'com' parent pacakge. Use of qualifiers to extend the accessibility of 
// members is generally useful. However, this example would only be of use if you needed to make the method accessible 
// to all commercial classes but not non-commercial ones (e.g. in org root packages) - which is a bit odd...