/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 7. Packages and Imports, Exercise 3.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write a package 'random' with functions nextInt(): Int, nextDouble(): Double and setSeed(seed:Int): Unit. 
// To generate random numbers, use the linear congruential generator: next = previous * a + b mod 2n, where a=1664525, 
// b=1013904223, and n = 32.

// Packages can contain classes, objects and traits, but not definitions of functions or variables...
package com.neiljbrown.scalaforimpatient.chapter7

// ...Therefore, use a Scala package object to implement the required utility functions
// Package objects are declared in the parent package (in this case com.neiljbrown.scalaforimpatient) with the same name 
// as the child package (in this case 'random').
// Scala compiles a package object into a JVM class named package.class with static methods and fields. In our case 
// this will be class com.neiljbrown.scalaforimpatient.chapter7.random.package.class
package object random {

  private val A = 1664525
  private val B = 1013904223
  private val N = 32 

  private var previous: Double = 0.0

  def nextInt: Int = {
    this.generateRandom.toInt
  }

  def nextDouble: Double = {
  	this.generateRandom
  }

  def setSeed(seed: Int): Unit = {
    this.previous = seed
  }

  private def generateRandom: Double = {
    val next: Double = (this.previous * A + B) % Math.pow(2,N)
    this.previous = next
    next
  }
}

object Exercise3 extends App {
  // Import statements can be used at any position
  import com.neiljbrown.scalaforimpatient.chapter7
  random.setSeed(123)
  for (i <- 1 to 10) {
    println(i + ") Generated next int [" + random.nextInt + "]")
  }
  
  for (i <- 1 to 10) {
    println(i + ") Generated next double [" + random.nextDouble + "]")
  } 
}