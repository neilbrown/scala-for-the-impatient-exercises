/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 7. Packages and Imports, Exercise 2.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write a puzzler that baffles your Scala friends, using a package 'com' that isn't at the top level.
//
// A) The question is too vague. I've just done what it asks and declared a nested package in which 'com' isn't the 
// first path. This only serves to show that packages can have arbitrary names (as in Java) and that the directory
// structure does not need to match the package structure
//
// Compile this source using: scalac com/neiljbrown/scalaforimpatient/chapter<n>/Excercise<n>.scala
// Run the program using: scala scalaforimpatient.neiljbrown.com.Exercise2

package scalaforimpatient.neiljbrown.com
class AClass {}

object Exercise2 extends App {
  val a = new scalaforimpatient.neiljbrown.com.AClass
}