/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 7. Packages and Imports, Exercise 6.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write a program that copies all elements from a Java hash map into a Scala hash map. Use imports to rename both 
// classes.
package com.neiljbrown.scalaforimpatient.chapter7

// Use an import selector: {A => B} to rename Java’s HashMap to avoid conflict with the Scala one
import java.util.{ HashMap => JavaHashMap }
// Also rename the Scala HashMap on import. This is unnecessary, but it's what the question asks for
import scala.collection.mutable.{ HashMap => MutableHashMap }

object Exercise6 extends App {
  val scores = new JavaHashMap[String, Int]
  scores.put("Alice", 10)
  scores.put("Bob", 3)
  scores.put("Cindy", 8)

  // Note - Would've typically used JavaConversions.mapAsScalaMap to automate conversion of Java map to Scala map, 
  // however, this only appears to work when converting to a mutable Scala Map (abstract) and not a concrete Scala HashMap 
  // as required in this exercise. Therefore adopted an alternative solution based on instantiating the Scala map
  import scala.collection.JavaConversions._
  val originalScores = new MutableHashMap[String, Int]
  originalScores.putAll(scores)

  assert(originalScores == Map("Alice" -> 10, "Bob" -> 3, "Cindy" -> 8))
}