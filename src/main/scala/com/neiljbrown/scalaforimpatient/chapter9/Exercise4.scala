/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 9. Files and Regular Expressions, Exercise 4.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write a Scala program that reads a text file containing only floating-point numbers. Print the sum, average, 
// maximum, and minimum of the numbers in the file.
package com.neiljbrown.scalaforimpatient.chapter9

import java.io.File
import java.io.FileNotFoundException
import scala.collection.mutable.ArrayBuffer
import scala.io.Source

/**
 * Program that reads a text file identified by a supplied path containing space delimited floating-point numbers and
 * prints the sum, average, maximum and minimum of the numbers.
 */
object Exercise4 extends App {
  val filepath = if (args.length == 1) args(0) else Console.readLine("Enter path of text file to convert or CTRL+C to quit: ")
  val file = new File(filepath)
  if (!file.exists() || !file.canWrite()) {
    System.err.println("File [" + filepath + "] must exist and be writeable.")
    sys.exit(1)
  }
  println("Processing file [" + filepath + "]...")
  val numbers = Source.fromFile(filepath, "UTF-8").mkString.split("\\s+").map(_.toFloat)
  val sum = numbers.sum
  println("Sum of numbers [" + sum + "]")
  println("Average [" + sum / numbers.length + "]")
  println("Max [" + numbers.max + "]")
  println("Min [" + numbers.min + "]")
}