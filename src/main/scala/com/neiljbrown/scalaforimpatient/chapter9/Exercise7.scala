/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 9. Files and Regular Expressions, Exercise 7.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write a Scala program that reads a text file and prints all tokens in the file that are not floating-point 
// numbers. Use a regular expression.
package com.neiljbrown.scalaforimpatient.chapter9

import scala.io.Source

/**
 * Program that reads a text file and prints all tokens in the file that are not floating-point numbers to stdout.
 */
object Exercise7 extends App {
  val filepath = if (args.length == 1) args(0) else Console.readLine("Enter path of text file to scan or CTRL+C to quit: ")
  val file = new java.io.File(filepath)
  if (!file.exists() || !file.canRead()) {
    System.err.println("File [" + filepath + "] must exist and be readable.")
    sys.exit(1)
  }
  println("Scanning file [" + filepath + "] for tokens that are NOT floating point numbers...")
  // scala.util.matching.Regex objects are constructed using the r method of the String class, but for this answer
  // we only need to use String.matches() which accepts a string (non-compiled) pattern
  val floatingPointNumPattern = "^[0-9]*\\.[0-9]+$"
  val tokens = Source.fromFile(filepath, "UTF-8").mkString.split("\\s+")
  var count = 0
  for (token <- tokens if !token.matches(floatingPointNumPattern)) {
    if (count > 0) print(" " + token) else print(token)
    count += 1
  }
  println("\nFinished. Found and filtered out [" + count + "] floating point numbers.")
}