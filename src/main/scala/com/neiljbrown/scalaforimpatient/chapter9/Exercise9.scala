/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 9. Files and Regular Expressions, Exercise 9.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write a Scala program that counts how many files with a .class extension are in a given directory and its sub dir.
package com.neiljbrown.scalaforimpatient.chapter9

import java.io.File
import scala.io.Source

/**
 * Program that counts the no. of .class files in a given directory and its sub-directories.
 */
object Exercise9 extends App {
  val filepath = if (args.length == 1) args(0) else Console.readLine("Enter a directory or CTRL+C to quit: ")
  val file = new java.io.File(filepath)
  if (!file.exists() || !file.canRead() || !file.isDirectory) {
    System.err.println("File [" + filepath + "] must be a readable directory.")
    sys.exit(1)
  }
  val classFilePattern = ".*\\.class"
  var totalClassFiles = matchFilesByName(file, classFilePattern).length
  // Debug - uncomment to list matched file names
  //println(matchFilesByName(file,classFilePattern).mkString("\n"))
  for (subDir <- subdirs(file)) {
    totalClassFiles += matchFilesByName(subDir, classFilePattern).length
    // Debug - uncomment to list matched file names  	
    //println(matchFilesByName(subDir,classFilePattern).mkString("\n"))  	
  }
  println("Found [" + totalClassFiles + "] class files below directory [" + filepath + "].")

  /**
   * Function which returns an iterator through all sub directories in a specified dir.
   * <p>
   * There are currently no official Scala classes for visiting all files in a dir, or for recursively traversing dirs.
   * Writing a function such as this is one of a couple of possible solutions.
   *
   * @param dir
   * @return An Iterator over zero or more files.
   */
  def subdirs(dir: File): Iterator[File] = {
    val children = dir.listFiles.filter(_.isDirectory)
    children.toIterator ++ children.toIterator.flatMap(subdirs _)
  }

  /**
   * Returns all the files in a directory whose names match a specified pattern.
   *
   * @param dir The directory.
   * @pattern pattern The string pattern for the file name.
   */
  def matchFilesByName(dir: File, pattern: String): Array[File] = {
    dir.listFiles.filter(_.isFile).filter(_.getName.matches(pattern))
  }
}