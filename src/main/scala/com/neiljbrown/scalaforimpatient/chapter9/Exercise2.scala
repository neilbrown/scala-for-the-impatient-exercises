/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 9. Files and Regular Expressions, Exercise 2.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write a scala program that reads a file with tabs, replaces each tab with spaces so that tab stops are at 
// n-column boundaries, and writes the result to the same file.
package com.neiljbrown.scalaforimpatient.chapter9

import scala.Console
import java.io._
import scala.io.Source

/**
 * Programm entry point. Validates a single command line argument is supplied which identifies a writeable file before
 * passing that file for conversion.
 */
object Exercise2 extends App {
  val filepath = if (args.length == 1) args(0) else Console.readLine("Enter path of text file to convert or CTRL+C to quit: ")
  val file = new File(filepath)
  if (!file.exists() || !file.canWrite()) {
    System.err.println("File [" + filepath + "] must exist and be writeable.")
    sys.exit(1)
  }
  println("Converting tabs to spaces in file [" + filepath + "]...")
  new Tabs2SpacesFileConverter(file).convert()
  println("...Done.")
}

/**
 * Converts tab characters in an identified text file to spaces and writes the results to the same file.
 *
 * @param inputFile The file to convert.
 */
class Tabs2SpacesFileConverter(val inputFile: File) {
  // Note - Scala doesn't require you to declare checked exceptions thrown by methods, nor does it seem to support 
  // declaring them at all, at least not on primary constructors
  if (!this.inputFile.exists()) throw new FileNotFoundException("File [" + this.inputFile + "] does not exist.")
  else if (!this.inputFile.canWrite())
    throw new IllegalArgumentException("File [" + this.inputFile + "] must be writeable.")

  /**
   * Converts the file supplied on construction.
   */
  def convert() {
    // Read the entire contents of the file into a string and replace all tabs with spaces
    // Note, another way to do this with less memory overhead would be to read the input file one character at a time 
    // using the Source returned from Source.fromFile() as a character iterator, for (c <- source)
    val convertedContents = Source.fromFile(this.inputFile, "UTF-8").mkString.replaceAll("\\t", " ")
    val outputFile = new PrintWriter(inputFile)
    outputFile.write(convertedContents)
    outputFile.close()
  }
}