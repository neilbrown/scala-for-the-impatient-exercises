/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 9. Files and Regular Expressions, Exercise 1.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write a Scala code snippet that reverses the lines in a file (making the last line the first one, and so on).
//
// A) Note, the following solution relies on reading the whole file into memory. A more efficient solution would only 
// read a maximum of one line into memory. This will require scanning to the end of file and reading backwards.
package com.neiljbrown.scalaforimpatient.chapter9

object Exercise1 extends App {
  val inputFilepath = Console.readLine("Enter the path of file whose contents should be reversed, or CTRL+C to quit: ")
  import scala.io.Source
  val source = Source.fromFile(inputFilepath, "UTF-8")

  val outputFilepath = inputFilepath + ".out"
  println("Contents of file [" + inputFilepath + "] will be reversed and written to [" + outputFilepath + "].")
  Console.readLine("Press return to continue, or CTRL+C to quit.")

  val lineIterator = source.getLines
  val reversedLinesArray = lineIterator.toArray.reverse
  import java.io.PrintWriter
  val outputFile = new PrintWriter(outputFilepath)
  for (nextLine <- reversedLinesArray) outputFile.println(nextLine)
  outputFile.close
  println("Done.")
}