/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 9. Files and Regular Expressions, Exercise 5.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write a Scala program that writes the powers of 2 and their reciprocals to a file, with the exponent ranging from 
// 0 to 20. Line up the columns.
package com.neiljbrown.scalaforimpatient.chapter9

import java.io.PrintWriter

object Exercise5 extends App {
  val outputFile = new PrintWriter("powersOfTwoAndReciprocals.txt")
  // Getting the desired formatted output (width and precision in both columns, and dropping the trailling zeros in the 
  // reciprocals) was a bit of a bugger. The solution is based on using StringOps.format() with careful chosen format 
  // specifiers, which are based on the Java ones - http://docs.oracle.com/javase/6/docs/api/java/util/Formatter.html
  // Useful resources on formatting:
  // http://stackoverflow.com/questions/1350566/number-formatting-in-scala
  // http://stackoverflow.com/questions/17089002/string-formatting-with-maximum-number-of-digits
  // http://stackoverflow.com/questions/277772/avoid-trailing-zeroes-in-printf
  for (exponent <- 0 to 20) outputFile.println("%10.0f".format(scala.math.pow(2, exponent)) + "%15.2g".format(1 / scala.math.pow(2, exponent)))
  outputFile.close
}