/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 9. Files and Regular Expressions, Exercise 8.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write a Scala program that prints the src attributes of all img tags of a web page. Use regular expressions and
// groups.
//
// A) Compile and run this answer as follows:
// scalac com/neiljbrown/scalaforimpatient/chapter9/Excercise8.scala
// scala com.neiljbrown.scalaforimpatient.chapter9.Excercise8 {URL}
package com.neiljbrown.scalaforimpatient.chapter9

import java.net._
import scala.io.Source

/**
 * Program that retrieves a web page and parses and prints the 'src' attributes of all its 'img' tags, i.e. the path
 * of all the images.
 */
object Exercise8 extends App {
  val enteredUrl = if (args.length == 1) args(0) else Console.readLine("Enter the URL of the web page or CTRL+C to quit: ")
  try {
    val url = new URL(enteredUrl)
    println("Getting URL [" + enteredUrl + "]...")
    // scala.io.Source provides methods to read from sources other than files, including URLs
    val webPage = Source.fromURL(enteredUrl, "UTF-8").mkString
    // Construct a scala.util.matching.Regex (compiled regex pattern) object using the r() method of the String class
    // Note the use of "?" after ".*" to force the regex engine to lazily rather than greedily match the contents 
    // between the double quotes, so it only matches the attributes of the 'src' attribute and any additional characters 
    // if the line includes other double quotes. Ref. http://www.regular-expressions.info/repeat.html
    val pattern = "<img.*src=\"(.*?)\"".r
    println("Parsing resource for img tags with src attributes...")
    for (pattern(srcAttributes) <- pattern.findAllIn(webPage)) println(srcAttributes)
  } catch {
    case _: MalformedURLException =>
      System.err.println("Invalid url [" + enteredUrl + "].")
      sys.exit(1)
  }
}