/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 9. Files and Regular Expressions, Exercise 10.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Expand the example with the serializable Person class that stores a collection of friends. Construct a few Person
// objects, make some of them friends of another, and then save an Array[Person] to a file. Read the array back in and 
// verify that the friend relations are intact.
//
// A) Note - The answers is implemented as a program because when implemented as a script Scala (2.10.2) throws a 
// java.io.NotSerializableException on out.writeObject(), even though the class being written extends Serializable?
package com.neiljbrown.scalaforimpatient.chapter9

import scala.collection.mutable.ArrayBuffer

/**
 * Program that illustrates how to serialise / deserialise objects in Scala, including Scala collection fields.
 */
object Exercise10 extends App {
  val joe = new Person("Joe", "Bloggs")
  val cathy = new Person("Katherine", "Cuthbert")
  val lionel = new Person("Lionel", "Domino")

  joe.addFriend(cathy)
  joe.addFriend(lionel)

  import java.io._
  val filePath = "/tmp/chapter9Exercise10.obj"
  val out = new ObjectOutputStream(new FileOutputStream(filePath))
  out.writeObject(joe)
  out.close

  val in = new ObjectInputStream(new FileInputStream(filePath))
  val savedJoe = in.readObject().asInstanceOf[Person]
  in.close

  assert(joe.firstName == "Joe")
  assert(joe.lastName == "Bloggs")
  assert(joe.friends.length == 2)
  assert(joe.friends(0).firstName == "Katherine")
  assert(joe.friends(1).firstName == "Lionel")
}

// The scala.Serializable trait is used to declare a class as serializable - can be ommitted if default UID acceptable
@SerialVersionUID(42L)
class Person(val firstName: String, val lastName: String) extends Serializable {
  val friends = new ArrayBuffer[Person]

  def addFriend(friend: Person) {
    this.friends += friend
  }
}