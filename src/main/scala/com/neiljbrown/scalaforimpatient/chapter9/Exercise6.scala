/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 9. Files and Regular Expressions, Exercise 6.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Make a regular expression searching for quoted strings "like this, maybe with \" or \\" in a Java or C++ program.
// Write a Scala program that prints out all such strings in a source file.
//
// A) Question is unclear. It's assumed the requirement is to write a Scala program which uses a regex to search for 
// and report quoted strings in a specified source file.
package com.neiljbrown.scalaforimpatient.chapter9

import scala.io.Source

/**
 * Program that searches for and reports all double-quoted strings in a specified text file.
 */
object Exercise6 extends App {
  val filepath = if (args.length == 1) args(0) else Console.readLine("Enter path of text file to scan or CTRL+C to quit: ")
  val file = new java.io.File(filepath)
  if (!file.exists() || !file.canRead()) {
    System.err.println("File [" + filepath + "] must exist and be readable.")
    sys.exit(1)
  }
  println("Scanning file [" + filepath + "] for quoted strings...")
  val fileContents = Source.fromFile(filepath, "UTF-8").mkString
  // Construct a scala.util.matching.Regex (compiled regex pattern) object using the r() method of the String class
  // Note the use of "?" after ".*" to force the regex engine to lazily rather than greedily match the contents 
  // between the double quotes, so it only matches up to the first quoted string and not additional quotes on same line.
  // Ref. http://www.regular-expressions.info/repeat.html
  val pattern = "(\".*?\")".r
  var count = 0
  for (pattern(quotedString) <- pattern.findAllIn(fileContents)) {
    println(quotedString)
    count += 1
  }
  println("Finished. Found [" + count + "] quoted strings.")
}