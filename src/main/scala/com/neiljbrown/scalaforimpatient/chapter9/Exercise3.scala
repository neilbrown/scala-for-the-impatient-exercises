/**
 * Scala for the Impatient, Exercises
 * https://bitbucket.org/neilbrown
 * Chapter 9. Files and Regular Expressions, Exercise 3.
 *
 * To execute the example, compile and run it by typing: scalac <filename>.scala; scala <classname>
 */
// Q) Write a Scala code snippet that reads a file and prints all words with more than 12 characters to the console. 
// Extra credit if you can do this in a single line.
package com.neiljbrown.scalaforimpatient.chapter9

object Exercise3 extends App {
  // The non-functional way
  for (word <- scala.io.Source.fromFile("chapter9Exercise3.scala", "UTF-8").mkString.split("\\s+") if word.length > 12) println(word)

  // The functional way
  //scala.io.Source.fromFile("chapter9Exercise3.scala", "UTF-8").mkString.split(" ").filter(_.length > 12).foreach(println)
}