My answers to the exercises at the end of each chapter of the Scala for the impatient book (2012) by Cay Horstmann.

The answers are implemented as Scala applications (classes with main methods) rather than scripts, as Scala IDEs 
currently provide much better support for the former than the latter. 

If you want to run the answers from the command line they needed to be compiled first. Generally speaking the commands 
required to compile and run each answer are:
$ cd {install-dir}/src/main/scala
$ scalac com/neiljbrown/scalaforimpatient/chapter<n>/Excercise<n>.scala
$ scala com.neiljbrown.scalaforimpatient.chapter<n>.Excercise<n>

Neil Brown.